﻿/* UserInterface.cs
 * Author: Matt Traudt
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Ksu.Cis300.WordLookup;

namespace Ksu.Cis300.CryptogramSolver
{
    /// <summary>
    /// User interface to a program that will
    /// solve cryptograms
    /// </summary>
    public partial class UserInterface : Form
    {
        private ITrie _dictionary;
        
        /// <summary>
        /// ctor
        /// </summary>
        public UserInterface()
        {
            InitializeComponent();
            _dictionary = new TrieWithNoChildren();
            try
            {
                using (StreamReader sr = File.OpenText("dictionary.txt"))
                {
                    while (!sr.EndOfStream)
                    {
                        _dictionary = _dictionary.Add(sr.ReadLine());
                    }
                }
                MessageBox.Show("Dictionary successfully read.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        /// <summary>
        /// Handles uxOpenFileButton click event.
        /// reads a file's text into uxCryptText
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uxOpenFileButton_Click(object sender, EventArgs e)
        {
            if (uxOpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string filename = uxOpenFileDialog.FileName;
                    using (StreamReader sr = new StreamReader(filename))
                    {
                        uxCryptText.Text = sr.ReadToEnd();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                
            }
        }

        /// <summary>
        /// Handles uxDecryptButton click event
        /// Prepares some variables and sends them 
        /// off to Decrypt function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uxDecryptButton_Click(object sender, EventArgs e)
        {
            string[] words = uxCryptText.Text.Split(' ');
            bool goodWords = true;
            for (int i = 0; i < words.Length; i++)
            {
                for (int j = 0; j < words[i].Length; j++)
                {
                    if (words[i][j] < 'a' || words[i][j] > 'z')
                    {
                        goodWords = false;
                        break;
                    }
                }
            }
            if (!goodWords)
            {
                uxDecryptText.Text = "Invalid characters. Only lowercase letters and spaces allowed.";
                return;
            }
            else
            {
                StringBuilder[] solvedWords = new StringBuilder[words.Length];
                for (int i = 0; i < solvedWords.Length; i++)
                {
                    solvedWords[i] = new StringBuilder(new string('?', words[i].Length));
                }
                if (Decrypt(words, solvedWords, new bool[26]))
                {
                    StringBuilder solution = new StringBuilder();
                    for (int i = 0; i < solvedWords.Length; i++)
                    {
                        solution.Append(solvedWords[i].ToString());
                        if (i + 1 < solvedWords.Length)
                            solution.Append(" ");
                    }
                    uxDecryptText.Text = solution.ToString();
                }
                else
                {
                    uxDecryptText.Text = "No soluions exist.";
                }
            }
        }

        /// <summary>
        /// Recursively try to decrypt a cipher
        /// </summary>
        /// <param name="cipher">cipher text to decrypt</param>
        /// <param name="partial">place to store working solution</param>
        /// <param name="alphaUsed">stores which letters have been used</param>
        /// <returns>true if can solve, otherwise false</returns>
        private bool Decrypt(string[] cipher, StringBuilder[] partial, bool[] alphaUsed)
        {
            ///////////////////////////////////////////////////////////////////////////// 
            // see if solved
            /////////////////////////////////////////////////////////////////////////////
            bool goodPartials = true;
            for (int i = 0; i < partial.Length; i++)
            {
                for (int j = 0; j < partial[i].Length; j++)
                {
                    if (partial[i][j] == '?')
                    {
                        // not yet solved (contains '?')
                        goodPartials = false;
                        break; 
                    }
                }
                if (!goodPartials) break;
                else if (!_dictionary.Contains(partial[i].ToString()))
                {
                    // not solved (no '?'s, but something not a word)
                    goodPartials = false;
                    break;
                }
            }
            if (goodPartials)
            {
                return true; // solved
            }
            /////////////////////////////////////////////////////////////////////////////
            // see if every partial has a possible completion
            /////////////////////////////////////////////////////////////////////////////
            for (int i = 0; i < partial.Length; i++)
            {
                if (!_dictionary.WildcardSearch(partial[i].ToString()))
                {
                    return false; // there was an unsolvable word
                }
            }
            /////////////////////////////////////////////////////////////////////////////
            // reconstruct the cipher array and the partial array into string(builders)
            // to make looping slightly easier at a little performance cost
            /////////////////////////////////////////////////////////////////////////////
            StringBuilder sbCiph = new StringBuilder();
            StringBuilder part = new StringBuilder();
            for (int i = 0; i < cipher.Length; i++)
            {
                sbCiph.Append(cipher[i]);
                if (i+1 < cipher.Length)
                    sbCiph.Append(" ");
                part.Append(partial[i].ToString());
                if (i+1 < cipher.Length)
                    part.Append(" ");
            }
            string ciph = sbCiph.ToString();
            
            // now work with string ciph (instead of string[] cipher)
            // now work with StringBuilder part (instead of StringBuilder[] partial)

            /////////////////////////////////////////////////////////////////////////////
            // loop over all chars in part, only looking at ?s
            /////////////////////////////////////////////////////////////////////////////
            for (int i = 0; i < part.Length; i++)
            {
                if (part[i] != '?') continue;
                /////////////////////////////////////////////////////////////////////////////
                // loop over letters a - z, consider only ones unused
                /////////////////////////////////////////////////////////////////////////////
                for (char l = 'a'; l - 'a' < alphaUsed.Length; l++)
                {
                    if (alphaUsed[l - 'a']) continue;
                    /////////////////////////////////////////////////////////////////////////////
                    // replace all ? that go with v in ciph with l
                    /////////////////////////////////////////////////////////////////////////////
                    char v = ciph[i];
                    int vindex = ciph.IndexOf(v);
                    while (vindex != -1)
                    {
                        part[vindex] = l;
                        vindex = ciph.IndexOf(v, vindex + 1);
                    }
                    /////////////////////////////////////////////////////////////////////////////
                    // see if that solved the puzzle
                    /////////////////////////////////////////////////////////////////////////////
                    alphaUsed[l - 'a'] = true;
                    cipher = ciph.Split(' ');
                    //string[] ciphToPass = ciph.Split(' ');
                    //StringBuilder[] partToPass = new StringBuilder[cipher.Length];
                    string[] parts = part.ToString().Split(' ');
                    for (int j = 0; j < partial.Length; j++)
                        partial[j] = new StringBuilder(parts[j]);
                    if (Decrypt(cipher, partial, alphaUsed))
                        return true;
                    else
                    {
                        /////////////////////////////////////////////////////////////////////////////
                        // didn't solve, undo replacements
                        /////////////////////////////////////////////////////////////////////////////
                        vindex = ciph.IndexOf(v);
                        while (vindex != -1)
                        {
                            part[vindex] = '?';
                            vindex = ciph.IndexOf(v, vindex + 1);
                        }
                        alphaUsed[l - 'a'] = false;
                    }
                }
                /////////////////////////////////////////////////////////////////////////////
                // tried all letters, didn't work so nothing will
                /////////////////////////////////////////////////////////////////////////////
                if (part[i] == '?') return false;
            }
            /////////////////////////////////////////////////////////////////////////////
            // never reaches here
            /////////////////////////////////////////////////////////////////////////////
            return false;
        }
        
    }
}
