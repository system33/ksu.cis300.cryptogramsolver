﻿namespace Ksu.Cis300.CryptogramSolver
{
    partial class UserInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uxCryptText = new System.Windows.Forms.TextBox();
            this.uxOpenFileButton = new System.Windows.Forms.Button();
            this.uxDecryptButton = new System.Windows.Forms.Button();
            this.uxDecryptText = new System.Windows.Forms.TextBox();
            this.uxOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // uxCryptText
            // 
            this.uxCryptText.Location = new System.Drawing.Point(12, 12);
            this.uxCryptText.Multiline = true;
            this.uxCryptText.Name = "uxCryptText";
            this.uxCryptText.Size = new System.Drawing.Size(363, 109);
            this.uxCryptText.TabIndex = 0;
            // 
            // uxOpenFileButton
            // 
            this.uxOpenFileButton.Location = new System.Drawing.Point(12, 127);
            this.uxOpenFileButton.Name = "uxOpenFileButton";
            this.uxOpenFileButton.Size = new System.Drawing.Size(75, 23);
            this.uxOpenFileButton.TabIndex = 1;
            this.uxOpenFileButton.Text = "Open File";
            this.uxOpenFileButton.UseVisualStyleBackColor = true;
            this.uxOpenFileButton.Click += new System.EventHandler(this.uxOpenFileButton_Click);
            // 
            // uxDecryptButton
            // 
            this.uxDecryptButton.Location = new System.Drawing.Point(300, 127);
            this.uxDecryptButton.Name = "uxDecryptButton";
            this.uxDecryptButton.Size = new System.Drawing.Size(75, 23);
            this.uxDecryptButton.TabIndex = 2;
            this.uxDecryptButton.Text = "Decrypt";
            this.uxDecryptButton.UseVisualStyleBackColor = true;
            this.uxDecryptButton.Click += new System.EventHandler(this.uxDecryptButton_Click);
            // 
            // uxDecryptText
            // 
            this.uxDecryptText.Location = new System.Drawing.Point(12, 156);
            this.uxDecryptText.Multiline = true;
            this.uxDecryptText.Name = "uxDecryptText";
            this.uxDecryptText.ReadOnly = true;
            this.uxDecryptText.Size = new System.Drawing.Size(363, 118);
            this.uxDecryptText.TabIndex = 3;
            // 
            // uxOpenFileDialog
            // 
            this.uxOpenFileDialog.FileName = "openFileDialog1";
            // 
            // UserInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(387, 286);
            this.Controls.Add(this.uxDecryptText);
            this.Controls.Add(this.uxDecryptButton);
            this.Controls.Add(this.uxOpenFileButton);
            this.Controls.Add(this.uxCryptText);
            this.Name = "UserInterface";
            this.Text = "Cryptogram Solver";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox uxCryptText;
        private System.Windows.Forms.Button uxOpenFileButton;
        private System.Windows.Forms.Button uxDecryptButton;
        private System.Windows.Forms.TextBox uxDecryptText;
        private System.Windows.Forms.OpenFileDialog uxOpenFileDialog;
    }
}

